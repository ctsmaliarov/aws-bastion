#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (c) 2018, Wouter de Geus <wdegeus(at)mirabeau.nl>

DOCUMENTATION = '''
lookup: aws_ondemand_price
author:
  - Wouter de Geus <wdegeus(at)mirabeau.nl>
version_added: 2.8
requirements:
  - boto3
  - botocore
short_description: Get the current ondemand price for an AWS EC2 Ondemand instance in USD.
description:
  - Get the current ondemand price foran AWS EC2 Ondemand instance in USD/hour.
    Arguments passed to this lookup module are instance types.
  - Make sure to pass the region to the desired lookup region. Since AWS only provides two
    endpoints for their pricing API we currently always use us-east-1 as endpoint.
    If no region is specified we default to eu-west-1.
  - When looking up a multiple instance types a dictionary will be returned with the price
    per instance type.
    For a single instance type the price is directly returned. See examples.

'''


EXAMPLES = '''
# lookup sample:
- name: lookup m5.large price in the us-east-1 region
  debug: msg="{{ lookup('aws_ondemand_price', 'm5.large', region=us-east-1 ) }}"

- name: lookup several instance types in in the eu-west-2 region
  debug: msg="{{ lookup('aws_ssm', 'm5.large', 'm4.large', 't3.nano', region='eu-west-2' ) }}"

'''

__metaclass__ = type

from ansible.module_utils._text import to_native
from ansible.module_utils.ec2 import HAS_BOTO3, boto3_tag_list_to_ansible_dict
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from pprint import pformat
import json

try:
    from botocore.exceptions import ClientError
    import botocore
    import boto3
except ImportError:
    pass  # will be captured by imported HAS_BOTO3

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

def _boto3_conn(credentials):
    if 'boto_profile' in credentials:
        boto_profile = credentials.pop('boto_profile')
    else:
        boto_profile = None

    try:
        connection = boto3.session.Session(profile_name=boto_profile).client('pricing', 'us-east-1', **credentials)
    except (botocore.exceptions.ProfileNotFound, botocore.exceptions.PartialCredentialsError):
        if boto_profile:
            try:
                connection = boto3.session.Session(profile_name=boto_profile).client('cloudformation', 'us-east-1')
            except (botocore.exceptions.ProfileNotFound, botocore.exceptions.PartialCredentialsError):
                raise AnsibleError("Insufficient credentials found.")
        else:
            raise AnsibleError("Insufficient credentials found.")
    return connection

'''
Function to fetch current ondemand price for instancetype/region

Ondemand prices are gathered through the AWS Pricing API, which is as of now only available in us-east-1 and ap-something, so we're picking the US endpoint.
# See https://aws.amazon.com/blogs/aws/aws-price-list-api-update-new-query-and-metadata-functions/ for details.

'''

regionmap = {
  "ap-northeast-1": "Asia Pacific (Tokyo)",
  "ap-northeast-2": "Asia Pacific (Seoul)",
  "ap-northeast-3": "Asia Pacific (Osaka-Local)",
  "ap-south-1"    : "Asia Pacific (Mumbai)",
  "ap-southeast-1": "Asia Pacific (Singapore)",
  "ap-southeast-2": "Asia Pacific (Sydney)",
  "ca-central-1"  : "Canada (Central)",
  # "cn-north-1"    : "China",
  # "cn-northwest-1": "China",
  "eu-central-1"  : "EU (Frankfurt)",
  "eu-west-1"     : "EU (Ireland)",
  "eu-west-2"     : "EU (London)",
  "eu-west-3"     : "EU (Paris)",
  "sa-east-1"     : u'South America (São Paulo)',
  "us-east-1"     : "US East (N. Virginia)",
  "us-east-2"     : "US East (Ohio)",
  "us-west-1"     : "US West (N. California)",
  "us-west-2"     : "US West (Oregon)",
}

def getOndemandPrice(pricing, instances, region):
  if not region in regionmap:
    print("Unknown region [{}], edit this script!".format(region))
    return -1
  location = regionmap[region]
  results = {}
  for instance_type in instances:
    response = pricing.get_products(
      ServiceCode='AmazonEC2',
      Filters = [
        {'Type' :'TERM_MATCH', 'Field':'instanceType', 'Value': instance_type },
        {'Type' :'TERM_MATCH', 'Field':'location'    , 'Value': location      },
        {'Type' :'TERM_MATCH', 'Field':'tenancy'     , 'Value':'shared'       },
        {'Type' :'TERM_MATCH', 'Field':'operation'   , 'Value':'RunInstances' },
      ],
      MaxResults=1
    )
    
    # We get a list of products with attributes and along with those are one or more 'terms' with pricing. For example Reserved Instance options and Ondemand.
    # For now assume we only care about the first (should be only) entry in the list, pick the ondemand one.
    for price in response['PriceList']:
      price = json.loads(price)
      ondemandprice = ''
      # Debug:
      # print(pformat(price['product']['attributes']))
      for p in price['terms']['OnDemand']:
        for dim in price['terms']['OnDemand'][p]['priceDimensions']:
          ondemandprice = price['terms']['OnDemand'][p]['priceDimensions'][dim]['pricePerUnit']['USD']
      results[instance_type] = ondemandprice
  return results

class LookupModule(LookupBase):
    def run(self, terms, variables=None, boto_profile=None, aws_profile=None,
            aws_secret_key=None, aws_access_key=None, aws_security_token=None, region=None):
        '''
            :arg terms: a list of lookups to run.
                e.g. ['m5.large', 'm4.large' ]
            :kwarg variables: ansible variables active at the time of the lookup
            :kwarg aws_secret_key: identity of the AWS key to use
            :kwarg aws_access_key: AWS seret key (matching identity)
            :kwarg aws_security_token: AWS session key if using STS
            :kwarg region: AWS region to do the lookup for
            :returns: When single instance_type is requested a single price is returned, otherwise a dict with instancetype:value is returned
        '''

        if not HAS_BOTO3:
            raise AnsibleError('botocore and boto3 are required for aws ondemand price lookup.')

        credentials = {}
        if aws_profile:
            credentials['boto_profile'] = aws_profile
        else:
            credentials['boto_profile'] = boto_profile
        credentials['aws_secret_access_key'] = aws_secret_key
        credentials['aws_access_key_id'] = aws_access_key
        credentials['aws_session_token'] = aws_security_token

        client = _boto3_conn(credentials)
        if region is None:
          region = 'eu-west-1'
        results = getOndemandPrice(client, terms, region)

        display.vvv("OndemandPrice lookup found %d prices: %s" % (len(results), pformat(results)))
        # This allows quick lookups like supplying MyVar: "{{ lookup('aws_ondemand_price', 'm5.large', region='us-east-2') }}"
        if (len(results.keys()) == 1):
            return [results[terms[0]]]
        # If we have multiple results we return them in a hash, useful when getting a big list of used instances and setting that fact early in the playbooks.
        return [results]

