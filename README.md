AWS Bastion
===========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create a bastion host with autoscaling group in your public subnet with an elastic IP address that automatically re-attaches if the host is killed.

If you want your cluster to sleep during off-hours you can provide this role with `sandman_tags`, which will be applied to the provisioned AutoScaling Group.
For example:
```yaml
sandman_tags:
  mcf:scheduleDown    : "00 19 ? * MON-FRI *"
  mcf:scheduleUp      : "00 7 ? * MON-FRI *"
  mcf:scheduleTimeZone: "Europe/Amsterdam"
```
Will cause the ASG to shutdown at 19:00 during the week and start backup again at 07:00 during the week.
You can manually set the `mcf:skipSchedule` tag on an ASG to skip the sleep events (including wakeup!).
NOTE that it's also possible to set the `mcf:scheduleXXX` tags through `cloudformation_tags` but this will also include the tags on all other resources (such as the SecurityGroup), which pollutes resource tags and therefore is not recommended.

Resources created are:
* Elastic IP address
  * Policy that allows host to attach it
* Bastion host
  * Launch Configuration
  * Autoscaling Group
* IAM Role/Profile to read SSM parameters and let cfn-init function
* Security Group
  * Ingress port 22 open

This role expects your keypair to be already present.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
* aws-setup
* aws-utils
* aws-lambda
* aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
```
Role Defaults
-------------
```yaml
create_changeset   : True
debug              : False
cloudformation_tags: {}
sandman_tags       : {}
tag_prefix         : "mcf"
private_bucket_name: "{{ account_name }}-private"

aws_bastion_params:
  create_changeset: "{{ create_changeset }}"
  debug           : False

  ami             : "{{ amis['ec2_latest_v2_ami'] }}"
  instance_type   : "t3.nano"
  keypair_name    : "{{ environment_abbr }}-bastion"

  use_spot        : True
  create_keypair  : True
  enable_mfa      : False

  cfninit_bucket_name: "{{ private_bucket_name }}"
```
Example Playbooks
----------------
Rollout Bastion server using custom encrypted AMI
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    environment_type: "test"
    environment_abbr: "tst"
    aws_utils_params:
      ec2_encrypt_ami: True

  pre_tasks:
    - name: Get latest AWS AMI's
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
  roles:
    - aws-setup
    - aws-lambda
    - aws-vpc
    - env-acl
    - aws-bastion
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
